public class CommandService {
    private ScannerWrapper scanner;
    private DiapasonService diapasonService;


    public CommandService(ScannerWrapper scanner, DiapasonService diapasonService) {
        this.scanner = scanner;
        this.diapasonService = diapasonService;
    }

    public boolean setDiapason(DiapasonService diapasonService){

            System.out.println("Введите минимальное число >= 1");
            diapasonService.setMin(Integer.parseInt(scanner.getCommand()));
            System.out.println("Введите максимально число <= 500");
            diapasonService.setMax(Integer.parseInt(scanner.getCommand()));

        diapasonService.setDiapason(diapasonService.getMax() - diapasonService.getMin() +1 );
        if(diapasonService.getMin() < 1 || diapasonService.getMax() > 500){
            System.out.println("Число должно быть больше 0 и меньше 501\n");
            return false;
        }
        diapasonService.setArray(new boolean[diapasonService.getDiapason()]);
        return true;
    }

    public String getCommand(){
        System.out.print("Ваш диапазон от " + diapasonService.getMin() + " до " + diapasonService.getMax()+"\n"+
                "Введите команду\n" +
                "1 generate \n" +
                "2. help\n" +
                "3. exit\n");
        String nextCommand = null;
            nextCommand = scanner.getCommand();
        return nextCommand;
    }

    public void command(String command){
        switch(command){
            case "generate":
            case "1":
                if(hasNumber(diapasonService)){
                    System.out.println(generateRandomNumber(diapasonService));
                } else {
                    System.out.println("Все числа от [" + diapasonService.getMin() + " до " + diapasonService.getMax()
                            +"] были згенерированы");
                }
                break;
            case "help":
            case "2":
                System.out.println("1.Generate - вы выбираете дипазон из чисел, после чего вам рандомным образом " +
                        "показываются числа, которые не могут повторяться\n2.Help - путиводитель по программе\n" +
                        "3.Exit - выход из программы\n");
                break;
            case "exit":
            case "3":
                System.exit(0);
            default:
                System.out.println("Не правильно веденная команда");
                break;
        }
    }

    public int generateRandomNumber(DiapasonService diapasonService){
        int randomNumber;
        do {
            randomNumber = (int) (Math.random() * diapasonService.getDiapason() + diapasonService.getMin());
        } while(generatedNumber(randomNumber, diapasonService));
        System.out.print("Ваше число ");
        return randomNumber;
    }

    private boolean generatedNumber(int number, DiapasonService diapasonService){
        if(diapasonService.getArray()[number - diapasonService.getMin()]){
            return true;
        } else {
            diapasonService.getArray()[number - diapasonService.getMin()] = true;
            return false;
        }
    }

    private boolean hasNumber(DiapasonService diapasonService){
        for(int i = 0; i < diapasonService.getArray().length; i++){
            if(!diapasonService.getArray()[i]){
                return true;
            }
        }
        return false;
    }
}