public class DiapasonService {
    private int min;
    private int max;
    private int Diapason;
    private boolean[] Array;
    private CommandService commandService;

    public void setCommandService(CommandService commandService) {
        this.commandService = commandService;
    }

    public int getMin() {
        return min;
    }
    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }
    public void setMax(int max) {
        this.max = max;
    }

    public int getDiapason() { return Diapason; }
    public void setDiapason(int Diapason) { this.Diapason = Diapason; }

    public boolean[] getArray() {
        return Array;
    }
    public void setArray(boolean[] Array) {
        this.Array = Array;
    }

    public void run(){
        commandService.setDiapason(this);
        while(true){
            commandService.command(commandService.getCommand());
        }
    }
}