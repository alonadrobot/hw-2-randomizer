import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ScannerWrapper scannerWrapper = new ScannerWrapper(new Scanner(System.in));
        DiapasonService diapasonService = new DiapasonService();
        CommandService commandService = new CommandService(scannerWrapper, diapasonService);

        diapasonService.setCommandService(commandService);
        diapasonService.run();
    }
}