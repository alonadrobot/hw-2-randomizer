import java.util.Scanner;

public class ScannerWrapper {
    private final Scanner scanner;

    public ScannerWrapper(Scanner scanner) {
        this.scanner = scanner;
    }

    public String getCommand(){
        return scanner.next();
    }
    public int getNumber(){
        return scanner.nextInt();
    }
}
