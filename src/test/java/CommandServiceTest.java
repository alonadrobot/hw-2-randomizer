import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class CommandServiceTest {
    private final ScannerWrapper scannerWrapper=new ScannerWrapper(new Scanner(System.in));
    private final DiapasonService diapasonService = new DiapasonService();
    private final CommandService cut = new CommandService(scannerWrapper, diapasonService);
    static Arguments[] generateRandomNumberTestArgs(){
        int[] expected1={1,2,3,4,5,6,7,8,9,10};
        int[] expected2={1,2,3,4,5};
        return new Arguments[]{
                Arguments.arguments(1,10,expected1),
                Arguments.arguments(1,5,expected2)
        };
    }
    @ParameterizedTest
    @MethodSource("generateRandomNumberTestArgs")
    void generateRandomNumberTest(int min, int max,int[] expected){
        int Diapason = max - min + 1;
        boolean[] Array = new boolean[Diapason];
        diapasonService.setArray(Array);
        diapasonService.setMax(max);
        diapasonService.setMin(min);
        diapasonService.setDiapason(Diapason);

        int[] actualSequenceOfRandomNumber = new int[Diapason];
        for(int i = 0; i < Diapason; i++){
            actualSequenceOfRandomNumber [i] = cut.generateRandomNumber(diapasonService);
        }
        Arrays.sort(actualSequenceOfRandomNumber);

        assertArrayEquals(expected, actualSequenceOfRandomNumber);
    }
}